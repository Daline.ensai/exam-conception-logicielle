# Examen

## Modalités

### Contexte

Les modalités de cet examen sont proches de celles d'un compte-rendu de TP.  
Il vous est demandé de réaliser une application répondant à la spécification donnée.

### Calendrier

L'évaluation commence lors de la séance du lundi 10 mai après-midi. Vous y disposez de 3 heures pour commencer le projet.  
Le rendu final est exigé pour le vendredi 14 mai à 22h. Il n'est pas attendu une grande quantité de travail (environ 2 heures) entre ces 2 dates.  

### Rendu attendu

Le rendu attendu est un dépôt git **public**, hébergé sur **votre compte personnel** sur la plateforme de votre choix (par défaut, gitlab.com mais vous êtes libres de choisir une plateforme concurrente tel que github ou bitbucket).  
Le lien de votre dépôt git devra être envoyé par mail à `olivier.levitt@gmail.com` avant la deadline de vendredi 14 mai 22h.

### Critères d'évaluation

Les critères d'évaluation sont les suivants :

- Un dépôt git bien entretenu
  - Le dépôt est public
  - Les fichiers versionnés respectent les bonnes pratiques (texte brut, uniquement les sources pas les produits ...)
  - L'historique permet de retracer les différentes étapes du développement (pas un simple unique commit)
  - Bonus : utilisation opportune des branches, issues et merge requests
- Du python industriel
  - Les dépendances sont clairement listées et installables en une commande
  - Le projet est lançable avec le minimum d'opérations manuelles
- Des applicatifs fonctionnels:
  - Un webservice accessible via HTTP, après démarrage
  - Un client pouvant accéder au webservice développé
  - Mise en place du scénario client
  - Bonus: De la gestion de cas limites, documentée dans le projet via commentaires.
- De la documentation
  - L'objectif, l'organisation et le fonctionnement du code est décrit succintement dans une documentation intégrée proprement au dépôt git
  - Une partie "quickstart" (démarrage rapide) est présente dans la documentation pour indiquer les quelques commandes standards pouvant être utilisées pour lancer le code
- De la portabilité
  - Les éventuels paramètres de configuration du projet sont externalisés et surchargeables  
  - Le projet n'a pas d'adhérence à une machine en particulier (liens de fichiers en dur, adhérence à un système d'exploitation)
- De la qualité
  - Au moins un test unitaire est présent
  - La façon de lancer les tests est documentée
- De l'automatisation
  - Bonus : un pipeline s'exécute à chaque push et lance les tests unitaires présents dans le projet

### Conditions de l'examen

Les conditions de cet examen se veulent proches de vos futures conditions de travail professionnelles.  
Ainsi, vous avez accès à toutes les ressources que vous souhaitez (internet, stackoverflow, forums, chats). De même, la réutilisation de code est permise, tant que vous en respectez la license d'utilisation.  
Vous êtes aussi libres de choisir votre environnement de travail. Il vous est ainsi par exemple toujours possible d'accéder aux services en ligne de la plateforme SSPCloud : [Datalab](https://datalab.sspcloud.fr) et [Che](https://che.lab.sspcloud.fr) mais vous pouvez tout à fait travailler sur votre environnement local ou sur tout autre environnement qui vous conviendrait.
Enfin, Les professeurs sont mobilisables, dans la limite du raisonnable, pendant toute la période de l'examen. Evidemment, comme pour toute demande de débug, il est demandé de joindre le code correspondant (lien vers le dépôt git à jour), le résultat attendu et l'erreur rencontrée (message d'erreur + `stacktrace`).

## TP

Le principe de ce TP noté repose sur l'utilisation de l'api documentée ici:
https://deckofcardsapi.com/ 

Cette API met a disposition des [decks de 52 de cartes classiques](https://fr.wikipedia.org/wiki/Jeu_de_cartes_fran%C3%A7ais). Elle propose également de nombreuses opérations détaillées dans la documentation.

**Rappel: Validation**

Pour la validation de votre code, le dépot git mis en place au cours de ce tp sera cloné, les modalités d'installation seront suivies, le web service sera lancé puis le client sera utilisé. 

### Spécification

En utilisant les packages de votre choix il vous est demandé de mettre en place, en Python : 

#### **Un webservice**
Un webservice qui :

- Récupère un deck de l'api et renvoie son id : exposé en `GET` sur `/creer-un-deck/`
- Tire des cartes du deck (tirer x cartes du deck parmi les restantes) : exposé en `POST` sur `/cartes/{nombre_cartes}` avec un body : `{deck_id:str}`

**Remarque importante :**

Ce Webservice est lui même un client web pour l'api https://deckofcardsapi.com/. Pour ce TP, il faudra, via la documentation de l'API requêter l'api (via GET sur différents endpoints par exemple) pour mener à bien l'élaboration de votre web service.

> La gestion des cas limites (plus de cartes dans un deck, pas de deck et remélanger) est laissée à votre discrétion


#### **Un client de votre webservice**
Il est demandé de mettre en place un client pour votre webservice capable de : 
- Initialiser un deck par requête `HTTP` `GET` sur `/creer-un-deck/`, renvoyant l'id en tant que str
- Tirer des cartes du deck en cours en faisant une requête `HTTP` `POST` sur `/cartes/{nombre_cartes}` avec en body l'id sous forme {deck_id:str}
- Calculer, pour une liste de cartes* donnée en entrée le nombre de cartes de chaque couleur sous forme d'un dictionnaire.
> {"H":int,"S":int,"D":int,"C":int}
avec H le nombre de coeurs, S le nombre de piques, D le nombre de carreaux, C le nombre de trèfles.

*Une Carte est de la forme:
```json
{
"image": "https://deckofcardsapi.com/static/img/8C.png",
"value": "8",
"suit": "CLUBS",
"code": "8C"
}
```

**Scénario**: En utilisant les fonctions précédentes, mettre en place un scénario `executable` qui initialise un deck, tire 10 cartes et compte le nombre de cartes tirées de chaque couleur.

Remarques : 

> La fonctionnalité de comptage se prête bien aux tests unitaires.
    
> Bien que vous puissiez les mettre dans le même dépôt, la partie webservice et la partie client doivent être considérées comme des modules applicatifs **indépendants**. 

